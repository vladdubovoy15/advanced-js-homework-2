"use strict";


const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70,
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70,
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70,
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40,
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    },
];

document.getElementById("root").innerHTML = "<ul id='ul'></ul>"

books.forEach((e, i) => {
    try {
        checkValidity(e, i);
        document.getElementById('ul').innerHTML += `<li>Book №${i + 1}. Author: ${e.author} Name: ${e.name} Price: ${e.price}</li>`
    } catch (error) {
        console.log(error.message);
    }
});

function checkValidity({author, name, price}, i) {
    if (!author) {
        throw new Error(`Book №${i + 1}. There is no author mentioned`);
    }
    if (!name) {
        throw new Error(`Book №${i + 1}. There is no name mentioned`);
    }
    if (!price) {
        throw new Error(`Book №${i + 1}. There is no price mentioned`);
    }
}